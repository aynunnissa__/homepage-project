import axios from 'axios'

export default class BlogService {
	constructor() {
		this.blogServiceUrl = '/base/ideas'
	}

	getTopBlogs(currentPage) {
		return axios.get('/base/ideas', {
			params: {
				'page[number]': currentPage,
				'page[size]': 4,
				append: 'small_image,medium_image'
			}
		})
	}

	getAllBlogs(currentPage, additionalParams) {
		return axios.get('/base/ideas', {
			params: {
				'page[number]': currentPage,
				'page[size]': 10,
				...additionalParams,
				append: 'small_image,medium_image'
			}
		})
	}

	async initiateBlogsData(currentPage, shouldGetTopBlogs, sortValue) {
		let promises = []
		if (shouldGetTopBlogs && sortValue === '') {
			promises = [this.getTopBlogs(currentPage), this.getAllBlogs(currentPage)]
		} else if (sortValue !== '') {
			promises = [this.getAllBlogs(currentPage, { sort: sortValue })]
		} else {
			promises = [this.getAllBlogs(currentPage)]
		}
		try {
			const [data1, data2] = await Promise.all(promises)

			const otherData = {
				meta: data1.data.meta,
				links: data1.data.links
			}

			if (shouldGetTopBlogs && sortValue === '') {
				return {
					...otherData,
					highlightedArticle: data1.data.data.slice(1, 2),
					topArticles: [
						...data1.data.data.slice(0, 1),
						...data1.data.data.slice(2, 4)
					],
					otherArticles: data2.data.data.slice(4, 10)
				}
			} else {
				return {
					...otherData,
					highlightedArticle: [],
					topArticles: [],
					otherArticles: data1.data.data
				}
			}
		} catch (error) {
			console.error(error)
		}
	}
}
